# README #

Personal version control for development of solar car simulator "box" with Raspberry Pi for ECS 193A Solar Car Project.

# Current Status #

Working on two distinct parts:

1) Configuring up a Raspberry Pi to be able to produce the necessary PWM output signals to mimic the potential output signals from the solar car.

2) Creating a wrapper program around Solar Car Efficiency software provided by Dr.Nitta that will produce the simulated PWM output signals based off the output from the Solar Car Efficiency software.