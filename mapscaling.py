#
#   TTP Vehicle Load Class
#
import math
from ttputils import *
from efficiencymap import *

Done = False
while not Done:
    print "G - Scale gas engine"
    print "E - Scale electric motor"
    print "D - Scale diesel engine"
    print "------------------------"
    print "Q - Quit"
    InputLine = raw_input("Enter Selection: ")
    Selection = InputLine.lower()
    if('q' == Selection):
        Done = True
    elif('e' == Selection) or ('g' == Selection) or ('d' == Selection):
        while True:
            Power = raw_input("Enter Max Power in kW (enter to exit): ")
            if 0 == len(Power):
                break
            if not IsFloat(Power):
                print "Invalid power value",Power
                continue
            if (0.0 >= float(Power)) or (1000.0 <= float(Power)):
                print "Invalid power value",Power
                continue
            else:
                break
        if 0 == len(Power):
            continue
        if('e' == Selection):
            OutputName = str(int(Power)) + "kwelectricmotordata.csv"
            InputMap = EfficiencyMap("electricmotordata.csv")
            InputMap.ScaleMap((float(Power) * 1000.0)/InputMap.PeakPower)
            print "Saving electric motor map as",OutputName
            InputMap.Save(OutputName)
        elif('g' == Selection):
            OutputName = str(int(Power)) + "kwgasenginedata.csv"
            InputMap = EfficiencyMap("gasenginedata.csv",True,False,False)
            Scaling = (float(Power) * 1000.0)/InputMap.PeakPower
            InputMap.ScaleMap(Scaling)
            if "displacement" in InputMap.Parameters:
                InputMap.Parameters["displacement"] = float(InputMap.Parameters["displacement"]) * Scaling

            if "bore" in InputMap.Parameters:
                InputMap.Parameters["bore"] = float(InputMap.Parameters["bore"]) * math.sqrt(Scaling)
            
            print "Saving gas engine map as",OutputName
            InputMap.Save(OutputName)
        elif('d' == Selection):
            OutputName = str(int(Power)) + "kwdieselenginedata.csv"
            InputMap = EfficiencyMap("dieselenginedata.csv",True,False,False)
            Scaling = (float(Power) * 1000.0)/InputMap.PeakPower
            InputMap.ScaleMap(Scaling)
            if "displacement" in InputMap.Parameters:
                InputMap.Parameters["displacement"] = float(InputMap.Parameters["displacement"]) * Scaling

            if "bore" in InputMap.Parameters:
                InputMap.Parameters["bore"] = float(InputMap.Parameters["bore"]) * math.sqrt(Scaling)
            print "Saving diesel engine map as",OutputName
            InputMap.Save(OutputName)

