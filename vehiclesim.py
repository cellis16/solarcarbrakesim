#
#   TTP Vehicle Load Class
#
import math
from ttputils import *
from powertrain import *

CO2FuelLookup = {"gasoline":99.18,"biodiesel":83.25,"e85":76.9,"electricity":124.10}

# Initialize Powertrain
VehicleEngine = EngineEfficiencyMap("75kwgasenginedata.csv")
VehicleMotor = EfficiencyMap("15kwelectricmotordata.csv")
TractionBattery = Battery(17000.0,4000.0,0.60 * 4000.0)
VehicleTransmission = Transmission(0.87, (10000, 4.08, 2.72, 1.82, 1.45, 1.12, 0.87, 0.73, 0.58))
VehicleGlider = VehicleLoad()
VehicleGlider.Mass = 1200     # kg (this is just the glider mass, need to add in powertrain mass)
VehicleGlider.Area = 2.67     # m^2
VehicleGlider.CD = 0.295      # coefficient of drag
VehicleGlider.CRR = 0.008     # coefficient of rolling resistance   
VehicleGlider.TireRadius = 0.34                # m
VehicleGlider.DifferentialRatio = 3.06         # gear ratio from transmission output to wheel
VehicleGlider.DifferentialEfficiency = 0.99    # efficiency of differential
VehiclePowertrain = Powertrain(VehicleEngine, VehicleMotor, TractionBattery, VehicleTransmission, VehicleGlider)
print "Running city cycle."
# Open the file
OutputFile = open("output_city.csv", 'wb')
# Create a CSV writer from 
OutputWriter = csv.writer(OutputFile, quoting=csv.QUOTE_ALL)
# Output the header

# Time, Speed, Distance, Gear, Powertrain RPM
HeaderRow = ["Time","Speed","Distance","Gear","RPM"]
# Add Motor Power, Pack Energy, SOC, V, I
HeaderRow.extend(["Motor Power","Energy","SOC","V","I"])
# Add Fuel, HC', CO', NOx', PM', Heat, ICE Temp, Coolant Temp
HeaderRow.extend(["Fuel","HC'","CO'","NOx'","PM'", "Heat", "ICE Temp", "Coolant Temp"])
# Add HC, CO, NOx, PM
HeaderRow.extend(["HC","CO","NOx","PM", "Cat Temp"])

OutputWriter.writerow(HeaderRow)
# Open the drive cycle file
with open("citycycle.csv", 'rU') as DriveCycleFile:
    DriveCycleReader = csv.reader(DriveCycleFile, delimiter=',')
    Distance = 0.0
    ElectricityWh = 0.0
    FuelGrams = 0.0
    HCGrams = 0.0
    COGrams = 0.0
    NOxGrams = 0.0
    PMGrams = 0.0
    # For each row in the CSV File
    for Row in DriveCycleReader:
        # Get time and speed
        TimestepData = VehiclePowertrain.CalculateTimestep(float(Row[1]), float(Row[0]))
        # Distance and electrical energy are cumulative
        Distance = TimestepData[2]
        ElectricityWh = TimestepData[6]
        # Grams are per timestep
        FuelGrams += TimestepData[10]
        HCGrams += TimestepData[18]
        COGrams += TimestepData[19]
        NOxGrams += TimestepData[20]
        PMGrams  += TimestepData[21]
        OutputWriter.writerow(TimestepData)
OutputFile.close()
CityOverview = [Distance, ElectricityWh, FuelGrams, HCGrams, COGrams, NOxGrams, PMGrams]

# Initialize Powertrain
VehiclePowertrain.Reset()
print "Running highway cycle."
# Open the file
OutputFile = open("output_highway.csv", 'wb')
# Create a CSV writer from 
OutputWriter = csv.writer(OutputFile, quoting=csv.QUOTE_ALL)
# Output the header
OutputWriter.writerow(HeaderRow)
# Open the drive cycle file
with open("highwaycycle.csv", 'rU') as DriveCycleFile:
    DriveCycleReader = csv.reader(DriveCycleFile, delimiter=',')
    Distance = 0.0
    ElectricityWh = 0.0
    FuelGrams = 0.0
    HCGrams = 0.0
    COGrams = 0.0
    NOxGrams = 0.0
    PMGrams = 0.0
    # For each row in the CSV File
    for Row in DriveCycleReader:
        # Get time and speed
        TimestepData = VehiclePowertrain.CalculateTimestep(float(Row[1]), float(Row[0]))
        # Distance and electrical energy are cumulative
        Distance = TimestepData[2]
        ElectricityWh = TimestepData[6]
        # Grams are per timestep
        FuelGrams += TimestepData[10]
        HCGrams += TimestepData[18]
        COGrams += TimestepData[19]
        NOxGrams += TimestepData[20]
        PMGrams += TimestepData[21]
        OutputWriter.writerow(TimestepData)
OutputFile.close()
HighwayOverview = [Distance, ElectricityWh, FuelGrams, HCGrams, COGrams, NOxGrams, PMGrams]
# Reinitialize Powertrain
VehiclePowertrain.Reset()
print "Calculating performance."
Time0to60 = VehiclePowertrain.Calculate0to60()
Grade6Speed = VehiclePowertrain.Calculate6PercentGradeSpeed()
# Open the file
OutputFile = open("output_overview.csv", 'wb')
# Create a CSV writer from 
OutputWriter = csv.writer(OutputFile, quoting=csv.QUOTE_ALL)
OutputWriter.writerow(["Mass", VehiclePowertrain.Vehicle.Mass, "kg"])
OutputWriter.writerow(["Area", VehiclePowertrain.Vehicle.Area, "m^2"])
OutputWriter.writerow(["CD", VehiclePowertrain.Vehicle.CD])
OutputWriter.writerow(["CRR", VehiclePowertrain.Vehicle.CRR])
OutputWriter.writerow(["0 to 60", Time0to60, "seconds"])
OutputWriter.writerow(["6% grade", Grade6Speed, "mph"])
OutputWriter.writerow(["", "City", "Highway", "Weighted"])
OutputWriter.writerow(["Distance (mi)", CityOverview[0], HighwayOverview[0], ""])
if VehiclePowertrain.EngineMap:
    OutputWriter.writerow(["Range (mi)", "N/A", "N/A", ""])   
else:
    OutputWriter.writerow(["Range (mi)", CityOverview[0] * (0.8 * TractionBattery.EnergyCapacity)/CityOverview[1] , HighwayOverview[0] * (0.8 * TractionBattery.EnergyCapacity)/HighwayOverview[1], ""])       
OutputWriter.writerow(["Electricity (MJ)", CityOverview[1] * 3600.0/1e6, HighwayOverview[1] * 3600.0/1e6, ""])
OutputWriter.writerow(["Fuel (g)", CityOverview[2], HighwayOverview[2], ""])
if VehiclePowertrain.EngineMap:
    OutputWriter.writerow(["Fuel (MJ)", CityOverview[2] * VehiclePowertrain.EngineMap.LHVJperg / 1.0e6, HighwayOverview[2] * VehiclePowertrain.EngineMap.LHVJperg / 1.0e6, ""])
else:
    OutputWriter.writerow(["Fuel (MJ)", "0.0", "0.0", ""])
CO2Fuel = CO2FuelLookup[VehiclePowertrain.FuelType()]
CityCO2 = CityOverview[1] * 3600.0/1e6 * CO2FuelLookup["electricity"]
HighwayCO2 = HighwayOverview[1] * 3600.0/1e6 * CO2FuelLookup["electricity"]
if VehiclePowertrain.EngineMap:
    CityCO2 += CityOverview[2] * VehiclePowertrain.EngineMap.LHVJperg / 1.0e6 * CO2Fuel
    HighwayCO2 += HighwayOverview[2] * VehiclePowertrain.EngineMap.LHVJperg / 1.0e6 * CO2Fuel
OutputWriter.writerow(["CO2 (g/mi)", CityCO2 / CityOverview[0], HighwayCO2 / HighwayOverview[0],  (CityCO2 / CityOverview[0]) * 0.55 +  (HighwayCO2 / HighwayOverview[0]) * 0.45 ])
CityHC = CityOverview[3] / CityOverview[0]
HighwayHC = HighwayOverview[3] / HighwayOverview[0]
OutputWriter.writerow(["HC (g/mi)", CityHC, HighwayHC, CityHC * 0.55 + HighwayHC * 0.45])
CityCO = CityOverview[4] / CityOverview[0]
HighwayCO = HighwayOverview[4] / HighwayOverview[0]
OutputWriter.writerow(["CO (g/mi)", CityCO, HighwayCO, CityCO * 0.55 + HighwayCO * 0.45])
CityNOx = CityOverview[5] / CityOverview[0]
HighwayNOx = HighwayOverview[5] / HighwayOverview[0]
OutputWriter.writerow(["NOx (g/mi)", CityNOx, HighwayNOx, CityNOx * 0.55 + HighwayNOx * 0.45])
CityPM = CityOverview[6] / CityOverview[0]
HighwayPM = HighwayOverview[6] / HighwayOverview[0]
OutputWriter.writerow(["PM (g/mi)", CityPM, HighwayPM, CityPM * 0.55 + HighwayPM * 0.45])
OutputWriter.writerow(["NMOG + NOx (g/mi)", CityHC + CityNOx, HighwayHC + HighwayNOx, (CityHC + CityNOx) * 0.55 + (HighwayHC + HighwayNOx) * 0.45])
OutputFile.close()
print "Done!"
