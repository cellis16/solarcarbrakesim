#
#   TTP 210 Utility Functions
#
import math
  
# Funciton to determine if string can be converted to float
def IsFloat(string):
    try:
        float(string)
        return True
    except ValueError:
        return False

# Funciton to determine if string can be converted to int
def IsInt(string):
    try:
        int(string)
        return True
    except ValueError:
        return False

# Function to convert RPM and torque to power
def RotationalPower(rpm, nm):
    return rpm * nm * 2 * math.pi / 60.0

# Function to convert Power and RPM to Torque
def RotationalTorque(power, rpm):
    return power / (rpm * 2 * math.pi / 60.0)

# Function to convert Power (W) and Time to Wh
def PowerTimeToWh(power,time):
    return power * ConvertSecondsToHours(time)

# Function to convert Power (W) and Time to kWh
def PowerTimeTokWh(power,time):
    return (power / 1000.0) * ConvertSecondsToHours(time)

# Function to convert Power (W) to (kW)
def ConvertWTokW(power):
    return power / 1000.0

# Function to convert Power (kW) to (W)
def ConvertkWToW(power):
    return power * 1000.0

# Function to convert Energy (J) to (kWh)
def ConvertJTokWh(energy):
    return energy / 3.6e6

# Function to convert Energy (kWh) to (J)
def ConvertkWhToJ(energy):
    return energy * 3.6e6

# Function to convert miles per hour to meters per second
def ConvertMPHToMPS(speed):
    return speed * 0.44704

# Function to convert meters per second to miles per hour
def ConvertMPSToMPH(speed):
    return speed * 2.2369362920544
    
# Function to convert meters to miles
def ConvertMetersToMiles(distance):
    return distance * 0.00062137119

# Function to convert miles to meters
def ConvertMilesToMeters(distance):
    return distance * 1609.344

# Function to convert seconds to hours
def ConvertSecondsToHours(time):
    return time / 3600.0

# Function to convert hours to seconds
def ConvertHoursToSeconds(time):
    return time * 3600.0

# Function to convert celcius to kelvin
def ConvertCToK(temp):
    return temp + 273.15

# Function to convert kelvin to celcius
def ConvertKToC(temp):
    return temp - 273.15

# Function to convert farenheit to kelvin
def ConvertFToK(temp):
    return ((temp - 32.0) * 5.0 / 9.0) + 273.15

# Function to convert kelvin to farenheit
def ConvertKToF(temp):
    return ((temp - 273.15) * 1.8) + 32.0

# Function to get gravity constant
def ConstantGravity():
    return 9.80665          # m/s^2
    
# Funciton to get standard air density
def ConstantAirDensity():
    return 1.3              # kg/m^3

# Funciton to get standard air density
def ConstantRinJmolK():
    return 8.3145           # J/mol K

# Function to convert volume to radius
def ConvertVolumeToRadius(volume):
    return ((volume * 0.75) / math.pi)**(1.0/3.0)

# Function to convert Liters to cm3
def ConvertLTocm3(volume):
    return (volume * 1000.0)

# Function to convert Liters to cm3
def Convertcm3ToL(volume):
    return (volume / 1000.0)

# Function to solve distance between two points
def CalculateDistance(x1, y1, x2, y2):
    DeltaX = x1 - x2
    DeltaY = y1 - y2
    return math.sqrt((DeltaX * DeltaX) + (DeltaY * DeltaY))

# Function to solve distance between two points
def CalculateDistancePoint(src, dest):
    return CalculateDistance(src[0],src[1],dest[0],dest[1])

# Function to solve M and B for line given two points
def LineCalculateMB(x1, y1, x2, y2):
    M = (y2 - y1)/(x2 - x1)
    B = y1 - M * x1
    return (M, B)

# Function to calculate distance between point and segment
def SegmentPointCalculateDistance(seg1,seg2,pt):
    DistEnds = min(CalculateDistancePoint(seg1,pt),CalculateDistancePoint(seg2,pt))
    
    # if vertical line
    if seg1[0] == seg2[0]:
        if (seg1[1] <= pt[1]) and (pt[1] <= seg2[1]):
            return abs(pt[0] - seg1[0])
        elif (seg1[1] >= pt[1]) and (pt[1] >= seg2[1]):
            return abs(pt[0] - seg1[0])
        else:
            return DistEnds
    # else if horizontal line
    elif seg1[1] == seg2[1]:
        if (seg1[0] <= pt[0]) and (pt[0] <= seg2[0]):
            return abs(pt[1] - seg1[1])
        elif (seg1[0] >= pt[0]) and (pt[0] >= seg2[0]):
            return abs(pt[1] - seg1[1])
        else:
            return DistEnds
    else:
        MBSeg = LineCalculateMB(seg1[0],seg1[1],seg2[0],seg2[1])
        MBTan = (-1.0/MBSeg[0], pt[1] + (1.0/MBSeg[0]) * pt[0] )
        InterX = (MBSeg[1]-MBTan[1])/(MBTan[0]-MBSeg[0])
        InterY = InterX * MBTan[0] + MBTan[1]
        return CalculateDistancePoint(pt, (InterX, InterY))

def CalculatePointLocation(src, dest, dist):
    # if vertical line
    if src[0] == dest[0]:
        return (src[0], src[1] + (dest[1] - src[1]) * dist) 
    else:
        MB = LineCalculateMB(src[0],src[1],dest[0],dest[1])
        X = src[0] + ((dest[0] - src[0]) * dist)
        return (X, MB[0] * X + MB[1])

# Solve the quadratic equation, return tuple of solutions
def QuadraticEquation(a, b, c):
    if(a == 0.0):
        RetVal = -c/b
        return (RetVal,)
    UnderSquareRt = b**2 - 4 * a * c
    if(UnderSquareRt == 0.0):
        return (-b/(2 * a))
    elif(UnderSquareRt < 0.0):
        return ()
    else:
        UnderSquareRt = math.sqrt(UnderSquareRt)
        PlusVal = (-b + UnderSquareRt)/(2 * a)
        MinusVal =(-b - UnderSquareRt)/(2 * a)
        return (PlusVal, MinusVal)

# Do interpolation based on x value lookup for y value
def Interpolate(x, xvals, yvals):
    if(x < xvals[0]):
        return yvals[0]
        
    for Index in range(1,len(xvals)):
        if((xvals[Index-1] <= x)and(x <= xvals[Index])):
            return yvals[Index-1] + (yvals[Index] - yvals[Index - 1])*(x - xvals[Index-1])/(xvals[Index] - xvals[Index -1])
        Index = Index + 1
    return yvals[-1]

# Clip value to within min/max
def ClipValue(val, minval, maxval):
    if(val < minval):
        return minval
    if(val > maxval):
        return maxval
    return val
    
# Find the array index that val is between
def ArrayValueToIndex(val, array):
    if(val < array[0]):
        return -1
    if(val > array[-1]):
        return len(array)
    for Index in range(1,len(array)):
        if((array[Index-1] <= val)and(val <= array[Index])):
            return Index -1
    return -2
    
# Find the array indices (float) that val is between
def ArrayValuesToIndices(val, array):
    ListOfIndices = []
    for Index in range(1,len(array)):
        if((array[Index-1] <= val)and(val <= array[Index])):
            Delta = array[Index] - array[Index-1]
            if(Delta > 0.0):
                ListOfIndices.append( float(Index-1) + (val - array[Index-1]) / Delta )
            else:
                ListOfIndices.append( float(Index-1) )
                ListOfIndices.append( float(Index) )
        elif((array[Index-1] >= val)and(val >= array[Index])):
            Delta = array[Index-1] - array[Index]
            ListOfIndices.append( float(Index-1) + (array[Index-1] - val) / Delta )
    return tuple( ListOfIndices )


def ConvertTimeToSeconds(timestr):
    if 3 > len(timestr):
        return -1
    timestr = timestr.strip()
    ColonIndex = timestr.find(":")
    if 0 > ColonIndex:
        return -1
    if IsInt(timestr[:ColonIndex]) and IsInt(timestr[ColonIndex+1:]):
        Hour = int(timestr[:ColonIndex])
        Minute = int(timestr[ColonIndex+1:])
        if 0 > Hour or 23 < Hour:
            return -1
        if 0 > Minute or 59 < Minute:
            return -1
        return Hour * 3600 + Minute * 60
    return -1

def ConvertSecondsToTime(timeinsec):
    Hour = timeinsec/3600
    Minute = (timeinsec/60) % 60
    if 10 > Hour:
        RetStr = '0' + str(Hour)
    else:
        RetStr = str(Hour)
    RetStr += ":"
    if 10 > Minute:
        RetStr += '0' + str(Minute)
    else:
        RetStr += str(Minute)
    return RetStr
    
def PrintListFixed(outlist, maxwidth, linewidth):
    OutString = '\n'
    ItemsPerLine = int(linewidth / (maxwidth + 1))
    ItemCount = 0
    for Item in outlist:
        ItemStr = Item
        for Index in range(len(ItemStr),maxwidth+1):
            ItemStr += ' '
        OutString += ItemStr
        ItemCount = ItemCount + 1
        if ItemCount >= ItemsPerLine:
            ItemCount = 0
            OutString += '\n'
    print OutString
    
def OutputHTMLHead(htmlfile,title):
    htmlfile.write('<!DOCTYPE html>\n<HTML>\n<HEAD>\n<TITLE>'+title+'</TITLE>\n</HEAD>\n<BODY>\n')

def OutputHTMLTail(htmlfile):
    htmlfile.write('</BODY>\n</HTML>\n')

def OutputHTMLHeader1(htmlfile,text):
    htmlfile.write('<H1>'+str(text)+'</H1>\n')

def OutputHTMLHeader2(htmlfile,text):
    htmlfile.write('<H2>'+str(text)+'</H2>\n')

def OutputHTMLIndent(htmlfile,spacestoadd):
    for Index in range(0,spacestoadd):
        htmlfile.write(' ')

def OutputHTMLTableBegin(htmlfile,border):
    if 0 < border:
        htmlfile.write('<TABLE BORDER="'+str(border)+'">\n')
    else:
        htmlfile.write('<TABLE>\n')

def OutputHTMLTableEnd(htmlfile):
    htmlfile.write('</TABLE>\n')

def OutputHTMLTableData(htmlfile, data, indent = 4):
    OutputHTMLIndent(htmlfile,indent)
    htmlfile.write('<TD>'+str(data)+'</TD>\n')
    
def OutputHTMLTableRow(htmlfile, row, indent = 2, align='center'):
    OutputHTMLIndent(htmlfile,indent)
    htmlfile.write('<TR ALIGN="'+str(align)+'">\n')
    for Item in row:
        OutputHTMLTableData(htmlfile,Item,indent*2)
    OutputHTMLIndent(htmlfile,indent)
    htmlfile.write('</TR>\n')

def OutputSVGHead(svgfile, width, height):
    svgfile.write('<SVG HEIGHT="'+str(height)+'" WIDTH="'+str(width)+'">\n')

def OutputSVGTail(svgfile):
    svgfile.write('</SVG>\n')

def OutputSVGLine(svgfile,src,dest,rgb,width):
    svgfile.write('<LINE X1="'+str(src[0])+'" Y1="'+str(src[1])+'" X2="'+str(dest[0])+'" Y2="'+str(dest[1])+'" STYLE="stroke:rgb('+str(rgb[0])+','+str(rgb[1])+','+str(rgb[2])+');stroke-width:'+str(width)+'" />\n')

def OutputSVGCircle(svgfile,center,radius,rgb):
    svgfile.write('<CIRCLE CX="'+str(center[0])+'" CY="'+str(center[1])+'" R="'+str(radius)+'" STYLE="stroke:rgb('+str(rgb[0])+','+str(rgb[1])+','+str(rgb[2])+');stroke-width:1;fill:rgb('+str(rgb[0])+','+str(rgb[1])+','+str(rgb[2])+')" />\n')

def OutputSVGSquare(svgfile,center,halfwidth,rgb):
    svgfile.write('<RECT X="'+str(center[0] - halfwidth)+'" Y="'+str(center[1] - halfwidth)+'" WIDTH="'+str(halfwidth*2)+'" HEIGHT="'+str(halfwidth*2)+'" STYLE="stroke:rgb('+str(rgb[0])+','+str(rgb[1])+','+str(rgb[2])+');stroke-width:1;fill:rgb('+str(rgb[0])+','+str(rgb[1])+','+str(rgb[2])+')" />\n')

